GCC=gcc

CFLAGS=-c -Wall -std=c++11
SOURCEDIR=src/
OUTDIR=build/
SOURCES=$(wildcard $(SOURCEDIR)*.cpp)
OBJECTS=$(SOURCES:$(SOURCEDIR)%.cpp=$(OUTDIR)%.o)
OUTPUT=$(OUTDIR)gamefile.a


all: $(OUTPUT)

$(OUTPUT): $(OBJECTS)
	ar rcs $(OUTPUT) $(OBJECTS)

$(OUTDIR)%.o:$(SOURCEDIR)%.cpp $(OUTDIR)
	$(GCC) $(CFLAGS) $< -o $@

$(OUTDIR):
	mkdir $(OUTDIR)


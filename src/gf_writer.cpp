#include "gf_writer.h"

#include <fstream>
#include <stack>

namespace gamefile
{
	namespace 
	{
		struct context
		{
			context(unsigned long start)
			{
				this->start = start;
			}

			unsigned long start;
		};
	}

	struct gfWriter::data
	{
		FILE *fs;
		std::stack<context> nesting;
		unsigned long lastFieldBegin;
	};

	gfWriter::gfWriter(std::string path)
	{
		this->dt = new data();
		this->dt->fs = fopen(path.c_str(),"wb");

		if (this->dt->fs == 0)
		{
			this->isValid = false;
			return;
		}

		this->isValid = true;
	}
	gfWriter::~gfWriter()
	{
		if (this->dt != 0)
		{
			Close();

			delete this->dt;
			this->dt = 0;
		}
	}

	int gfWriter::Close()
	{
		if (this->dt->fs != 0)
			fclose(this->dt->fs);
		this->dt->fs = 0;

		return GF_OK;
	}

	
	int gfWriter::BeginWriteField(gfFieldId id)
	{
		this->dt->lastFieldBegin = ftell(this->dt->fs);

		// Write id
		fwrite(&id,sizeof(gfFieldId),1,this->dt->fs);
		
		// Write placeholder for size (will be updated later)
		unsigned short plchld = 0;
		fwrite(&plchld,  sizeof(unsigned short),1,this->dt->fs);
		return GF_OK;
	}
	int gfWriter::WritePayload(void* data,unsigned short size)
	{
		//unsigned long cpos;
		fwrite(data,sizeof(unsigned char),size,this->dt->fs);
		//cpos = ftell(this->dt->fs);
		fseek(this->dt->fs,this->dt->lastFieldBegin + sizeof(gfFieldId),SEEK_SET);
		fwrite(&size,sizeof(unsigned short),1,this->dt->fs);
		fseek(this->dt->fs,0,SEEK_END);
		return GF_OK;
	}
	int gfWriter::BeginWriteChildren()
	{
		unsigned long cPos = ftell(this->dt->fs);
		this->dt->nesting.push(context(cPos));
		return GF_OK;
	}
	int gfWriter::EndWriteChildren()
	{
		unsigned long cPos = ftell(this->dt->fs);
		unsigned short lenght = (unsigned short)(cPos - this->dt->nesting.top().start);
		fseek(this->dt->fs,this->dt->nesting.top().start - sizeof(unsigned short), SEEK_SET);
		fwrite(&lenght,sizeof(unsigned short),1,this->dt->fs);
		this->dt->nesting.pop();
		fseek(this->dt->fs,0,SEEK_END);
		return GF_OK;
	}

	
	template<class T>
	int gfWriter::write(gfFieldId id,T v)
	{
		int err = 0;
		if ((err = this->BeginWriteField(id)) != GF_OK)
			return err;
		if ((err = this->WritePayload(&v, sizeof(v))) != GF_OK)
			return err;
		return GF_OK;
	}

	int gfWriter::WriteField(gfFieldId id,unsigned int v)
	{
		return this->write<unsigned int>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,         int v)
	{
		return this->write<int>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,unsigned short v)
	{
		return this->write<unsigned short>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,         short v)
	{
		return this->write<short>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,unsigned long v)
	{
		return this->write<unsigned long>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,         long v)
	{
		return this->write<long>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,         float v)
	{
		return this->write<float>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,         double v)
	{
		return this->write<double>(id,v);
	}
	int gfWriter::WriteField(gfFieldId id,std::string v)
	{
		int err = 0;
		if ((err = this->BeginWriteField(id)) != GF_OK)
			return err;
		if ((err = this->WritePayload((void*)v.c_str(), v.size())) != GF_OK)
			return err;
		return GF_OK;
	}
	int gfWriter::WriteField(gfFieldId id,bool v)
	{
		return this->write<bool>(id,v);
	}
}
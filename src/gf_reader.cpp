#include "gf_reader.h"

#include <fstream>
#include <stack>

namespace gamefile
{
	namespace 
	{
		struct context
		{
			context(unsigned long prevpos, unsigned long start, unsigned long length)
			{
				this->prevpos = prevpos;
				this->start = start;
				this->length = length;
			}

			unsigned long prevpos;
			unsigned long start;
			unsigned long length;
		};
	}

	struct gfReader::data
	{
		FILE *fs;
		std::stack<context> nesting;
	};

	gfReader::gfReader(std::string path)
	{
		this->dt = new data();
		this->dt->fs = fopen(path.c_str(),"rb");

		if (this->dt->fs == 0)
		{
			isValid = false;
			return;
		}
		isValid = true;
	}

	gfReader::~gfReader()
	{
		if (this->dt != 0)
		{
			Close();

			delete this->dt;
			this->dt = 0;
		}
	}

	int gfReader::Close()
	{
		if (this->dt->fs != 0)
			fclose(this->dt->fs);
		this->dt->fs = 0;

		return GF_OK;
	}
	
	int gfReader::ReadFieldHeader(gfFieldHeader &out)
	{
		unsigned long cPos = ftell(this->dt->fs);

		if (this->dt->nesting.size() > 0 && cPos >= this->dt->nesting.top().start + this->dt->nesting.top().length)
		{
			return GF_EOP;
		}

		// Read id and size of payload
		if (!fread(&out.id,sizeof(gfFieldId),1,this->dt->fs))
		{
			return GF_EOF;
		}
		if (!fread(&out.payload_size,sizeof(unsigned short),1,this->dt->fs))
		{
			return GF_EOF;
		}
		
		// Get the start position of the paylod in the file
		out.payload_pos = ftell(this->dt->fs);

		fseek(this->dt->fs,out.payload_pos + out.payload_size,SEEK_SET);

		return GF_OK;
	}

	int gfReader::ReadPayload(const gfFieldHeader &header,void *buffer)
	{
		unsigned long cPos = ftell(this->dt->fs);

		fseek(this->dt->fs,header.payload_pos,SEEK_SET);

		if (fread(buffer, sizeof(gfByte),header.payload_size,this->dt->fs) != header.payload_size)
		{
			fseek(this->dt->fs,cPos,SEEK_SET);
			return GF_EOF;
		}
		
		fseek(this->dt->fs,cPos,SEEK_SET);
		return GF_OK;
	}
	int gfReader::BeginReadChildFields(const gfFieldHeader &header)
	{
		unsigned long cPos = ftell(this->dt->fs);
		this->dt->nesting.push(context(cPos,header.payload_pos,header.payload_size));
		fseek(this->dt->fs,header.payload_pos,SEEK_SET);
		return GF_OK;
	}
	int gfReader::EndReadChildFields()
	{
		fseek(this->dt->fs,this->dt->nesting.top().prevpos,SEEK_SET);
		this->dt->nesting.pop();
		return GF_OK;
	}
	
	template<class T>
	T gfReader::read(const gfFieldHeader &header)
	{
		T res;
		this->ReadPayload(header,&res);
		return res;
	}

	char gfReader::ReadChar(const gfFieldHeader &head)
	{
		return read<char>(head);
	}
	unsigned char gfReader::ReadUChar(const gfFieldHeader& head)
	{
		return read<unsigned char>(head);
	}
	int gfReader::ReadInt(const gfFieldHeader& head)
	{
		return read<int>(head);
	}
	unsigned int gfReader::ReadUInt(const gfFieldHeader& head)
	{
		return read<unsigned int>(head);
	}
	short gfReader::ReadShort(const gfFieldHeader& head)
	{
		return read<short>(head);
	}
	unsigned short gfReader::ReadUShort(const gfFieldHeader& head)
	{
		return read<unsigned short>(head);
	}
	long gfReader::ReadLong(const gfFieldHeader& head)
	{
		return read<long>(head);
	}
	unsigned long gfReader::ReadULong(const gfFieldHeader& head)
	{
		return read<unsigned long>(head);
	}
	float gfReader::ReadFloat(const gfFieldHeader& head)
	{
		return read<float>(head);
	}
	double gfReader::ReadDouble(const gfFieldHeader& head)
	{
		return read<double>(head);
	}
	bool gfReader::ReadBool(const gfFieldHeader& head)
	{
		return read<bool>(head);
	}
	std::string gfReader::ReadString(const gfFieldHeader& head)
	{
		char *chars = new char[head.payload_size + 1];
		chars[head.payload_size] = 0;
		std::string res;
		this->ReadPayload(head,chars);
		res = std::string(chars);
		delete[] chars;
		return res;
	}
}
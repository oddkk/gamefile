#ifndef GF_READER_H
#define GF_READER_H

#include <iostream>
#include <string>

#include "gf_field.h"

#define GF_OK 0
#define GF_EOP 2
#define GF_ERROR 1
#define GF_EOF -1

namespace gamefile
{
	class gfReader
	{
	public:
		gfReader(std::string path);
		~gfReader();

		int Close();

		int ReadFieldHeader(gfFieldHeader &out);
		int ReadPayload(const gfFieldHeader &header,void *buffer);
		int BeginReadChildFields(const gfFieldHeader &header);
		int EndReadChildFields();
		
		 char ReadChar(const gfFieldHeader&);
unsigned char ReadUChar(const gfFieldHeader&);
		 int ReadInt(const gfFieldHeader&);
unsigned int ReadUInt(const gfFieldHeader&);
		 short ReadShort(const gfFieldHeader&);
unsigned short ReadUShort(const gfFieldHeader&);
		 long ReadLong(const gfFieldHeader&);
unsigned long ReadULong(const gfFieldHeader&);
		 float ReadFloat(const gfFieldHeader&);
		 double ReadDouble(const gfFieldHeader&);
		 bool ReadBool(const gfFieldHeader&);
		 std::string ReadString(const gfFieldHeader&);

		 bool isValid;
	private:
		struct data;
		data *dt;
		template<class T>
		T read(const gfFieldHeader &header);
	};
}

#endif
#ifndef GF_WRITER_H
#define GF_WRITER_H

#include <iostream>
#include <string>

#include "gf_field.h"

#define GF_OK 0
#define GF_EOP 2
#define GF_ERROR 1
#define GF_EOF -1

namespace gamefile
{
	class gfWriter
	{
	public:
		gfWriter(std::string path);
		~gfWriter();

		int Close();

		int BeginWriteField(gfFieldId);
		int WritePayload(void* data,unsigned short size);
		int BeginWriteChildren();
		int EndWriteChildren();

		int WriteField(gfFieldId,unsigned int);
		int WriteField(gfFieldId,         int);
		int WriteField(gfFieldId,unsigned short);
		int WriteField(gfFieldId,         short);
		int WriteField(gfFieldId,unsigned long);
		int WriteField(gfFieldId,         long);
		int WriteField(gfFieldId,         float);
		int WriteField(gfFieldId,         double);
		int WriteField(gfFieldId,std::string);
		int WriteField(gfFieldId,bool);
	
		bool isValid;
	private:
		struct data;
		data *dt;
		template<class T>
		int write(gfFieldId,T);
	};
}

#endif
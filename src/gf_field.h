#ifndef GF_FIELD_H
#define GF_FIELD_H

typedef unsigned short gfFieldId;
typedef unsigned char gfByte;

namespace gamefile
{
	struct gfFieldHeader
	{
		gfFieldId  id;
		unsigned short payload_size;
		unsigned long  payload_pos;
	};
}

#endif